﻿using Microsoft.Extensions.Configuration;
using SalesTaxAPI.DAL.Contracts;
using SalesTaxAPI.Helpers;
using SalesTaxAPI.Models;
using System.Collections.Generic;
using System.Linq;

namespace SalesTaxAPI.DAL.Services
{
    public class SalesTaxService: ISalesTaxService
    {
        private IHttpClientHelper httpClient;
        private ITaxCalculators taxCalculators;
        private IConfiguration _configs;
        private List<TaxCalculator> TaxCalculators;
        private string errorMsg = string.Empty;
        public SalesTaxService(IConfiguration configs)
        {            
            _configs = configs;
            TaxCalculators = new List<TaxCalculator>();
            configs.GetSection("TaxCalculators").Bind(TaxCalculators);
        }

        public TaxResponse<TaxOrderModel> CalculateTaxesForOrder(OrderTaxRequest request)
        {
            TaxResponse<TaxOrderModel> response = new TaxResponse<TaxOrderModel>();
            this.CreateTaxCalculator(request.TaxCalculatorId);

            if (!string.IsNullOrEmpty(this.errorMsg))
            {
                response.ErrorMessage = errorMsg;
                response.IsSucess = false;

                return response;
            }

            response.Result = taxCalculators.CalculateTaxesForOrder(request.OrderRequest);
            response.ErrorMessage = errorMsg;
            response.IsSucess = string.IsNullOrEmpty(errorMsg) ? true : false;

            return response;
        }

        public TaxResponse<TaxRateModel> GetTaxRatesForLocation(LocationTaxRequest request)
        {
            TaxResponse<TaxRateModel> response = new TaxResponse<TaxRateModel>();
            this.CreateTaxCalculator(request.TaxCalculatorId);

            if(!string.IsNullOrEmpty(this.errorMsg))
            {
                response.ErrorMessage = errorMsg;
                response.IsSucess = false;

                return response;
            }

            response.Result = taxCalculators.GetTaxRatesForLocation(request.LocationRequest);
            
            response.ErrorMessage = errorMsg;
            response.IsSucess = string.IsNullOrEmpty(errorMsg) ? true : false;

            return response;
        }

        private void CreateTaxCalculator(int taxCalculatorId)
        {
            var taxCalculator = TaxCalculators.FirstOrDefault(x => x.Id == taxCalculatorId);

            if (taxCalculator != null)
            {               
                if (httpClient == null)
                {
                    httpClient = new HttpClientHelper(taxCalculator);
                }

                switch (taxCalculatorId)
                {
                    case 1:

                        taxCalculators = new C1TaxCalculator(httpClient);
                        break;
                    case 2:

                        taxCalculators = new C2TaxCalculator(httpClient);
                        break;
                    default:
                        errorMsg = "Invalid TaxCalculator";
                        return;
                }
            }
            else
            {
                // Logging error
                errorMsg = "TaxAPI not available for the given TaxCalculator";
            }
        }

        public IHttpClientHelper clientHelper
        {
            get { return httpClient; }
            set { this.httpClient = value; }
        }       
    }
}
