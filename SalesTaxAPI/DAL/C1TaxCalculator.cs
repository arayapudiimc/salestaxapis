﻿using Newtonsoft.Json;
using SalesTaxAPI.DAL.Contracts;
using SalesTaxAPI.Models;
using System;

namespace SalesTaxAPI.DAL
{
    public class C1TaxCalculator : ITaxCalculators
    {
        IHttpClientHelper httpClient;
        public C1TaxCalculator(IHttpClientHelper client)
        {
            if(client == null)
            {
                throw new ArgumentNullException("client", "httpclient is null.");
            }

            httpClient = client;            
        }     

        public TaxOrderModel CalculateTaxesForOrder(OrderRequest request)
        {
            TaxOrderModel response = new TaxOrderModel();

            var result = httpClient.ExecutePost(request);
            response = JsonConvert.DeserializeObject<TaxOrderModel>(result);

            return response;
        }

        public TaxRateModel GetTaxRatesForLocation(LocationReqest request)
        {
            TaxRateModel response = new TaxRateModel();

             var result = httpClient.ExecuteGet(request);
            response = JsonConvert.DeserializeObject<TaxRateModel>(result);
           
            return response;
        }
    }
}
