﻿using SalesTaxAPI.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SalesTaxAPI.DAL.Contracts
{
    public interface ITaxCalculators
    {
        TaxRateModel GetTaxRatesForLocation(LocationReqest request);
        TaxOrderModel CalculateTaxesForOrder(OrderRequest request);
    }
}
